# App de Autentificación Presencial #

Aplicación de entorno *Android* Orientada a Autentificar usuarios de manera presencial a través de el escaneo de un código QR, mismo invocara una acción que a través de un *API* realizara la validación de las credenciales, devolviendo un booleano que definira si el cliente es Autentificado o no.